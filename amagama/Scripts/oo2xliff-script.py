#!"C:\Users\ritesh singh\PycharmProjects\amagama\amagama\Scripts\python.exe"
# EASY-INSTALL-ENTRY-SCRIPT: 'translate-toolkit==2.5.1','console_scripts','oo2xliff'
__requires__ = 'translate-toolkit==2.5.1'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('translate-toolkit==2.5.1', 'console_scripts', 'oo2xliff')()
    )
