argparse
blinker==0.9
Flask==0.8
psycopg2==2.2
translate-toolkit==1.9.0

# For better performance
# 0.11.0 is broken using pip, later versions are fixed
python-Levenshtein==0.10.2

# For management commands
Flask-Script==0.5.0

lxml
