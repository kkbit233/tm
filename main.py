


import logging
# from argparse import ArgumentParser

from translate.misc import wsgi

from amagama.application import amagama_server_factory

# @app.route("/")
# def hello():
    # return "Hello World fr/om Flask in a uWSGI Nginx Docker container with \
    #  Python 2.7 (default)"

# if __name__ == "__main__":
    # app.run(host='0.0.0.0', debug=True, port=80)



def main():

    # Setup logging.
    # if args.debug:
    level = logging.DEBUG
    format = ('%(levelname)7s %(module)s.%(funcName)s:%(lineno)d: '
                  '%(message)s')
    # else:
        # level = logging.WARNING
        # format = '%(asctime)s %(levelname)s %(message)s'

    logging.basicConfig(level=level, format=format)

    application = amagama_server_factory()
    wsgi.launch_server('0.0.0.0', '8888', application)


if __name__ == '__main__':
    main()
